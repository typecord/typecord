typecord [![NPM](https://img.shields.io/npm/v/typecord.svg)](https://www.npmjs.com/package/typecord) [![Discord](https://discordapp.com/api/guilds/508903834853310474/embed.png)](https://discord.gg/eaa5pYf) [![Install size](https://packagephobia.now.sh/badge?p=typecord)](https://packagephobia.now.sh/result?p=typecord)
===

A Discord library written in TypeScript

**This library is pretty early in development, so currently it doesn't do too much except connecting to the gateway and receiving packets**

Installation
---

With NPM

```
npm i typecord
```

With Yarn

```
yarn add typecord
```

Usage
---

```ts
// OPTIONAL: Install @ayana/logger and set LogLevel to TRACE to see some background info
import Logger, { LogLevel } from '@ayana/logger';
Logger.getDefaultTransport().setLevel(LogLevel.TRACE);

import { Shard, ShardManager } from 'typecord';

// For erlpack or zlib-sync to work, those dependencies have to be installed manually
const manager = new ShardManager('TOKEN HERE', { erlpack: false, compression: false });
const shard = new Shard(0, manager, payload => {
	// Most payloads land here. READY, RESUME, etc. are already handled by Typecord itself

	console.log(payload);

	// You could for example check for a message and then send it with a REST-Library for now
	// More stuff to come
});

manager.connect();
shard.connect();
```

Links
---

[GitLab repository](https://gitlab.com/typecord/typecord)

[NPM package](https://npmjs.com/package/typecord)

License
---

Refer to the [LICENSE](LICENSE) file.
