export enum MemberStatus {
	ONLINE = 'online',
	DND = 'dnd',
	IDLE = 'idle',
	INVISIBLE = 'invisible',
	OFFLINE = 'offline',
}
