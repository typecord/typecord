/**
 * WebSocket and Discord Gateway CloseCodes.
 * Also contains a code for no code (-1).
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/API/CloseEvent#Properties
 * @see https://discordapp.com/developers/docs/topics/opcodes-and-status-codes#gateway-gateway-close-event-codes
 */
export enum CloseCode {
	/**
	 * The close event had no code set.
	 */
	NO_CODE = -1,
	/**
	 * Regular socket shutdown.
	 */
	NORMAL = 1000,
	/**
	 * Discord doesn't know what went wrong. Try reconnecting?
	 */
	UNKNOWN = 4000,
	/**
	 * An invalid gateway OpCode was sent, or an invalid payload for the OpCode.
	 */
	UNKNOWN_OP_CODE = 4001,
	/**
	 * An invalid payload was sent.
	 */
	DECODE_ERROR = 4002,
	/**
	 * A payload was sent prior to identifying.
	 */
	NOT_AUTHENTICATED = 4003,
	/**
	 * The account token sent with the identify payload is incorrect.
	 */
	AUTHENTICATION_FAILED = 4004,
	/**
	 * More than one identify payload was sent.
	 */
	ALREADY_AUTHENTICATED = 4005,
	/**
	 * Probably used for telling the client that the session is invalid after resume.
	 * This code is not documented anymore but can apperantly still occur.
	 *
	 * zorikan <@138790157078888448> on Feb 18 2019 at 6:50 PM UTC: "4006 is still in our codebase, in a cursory inspection it looks like we can still generate it"
	 */
	RESUME_INVALID_SESSION = 4006,
	/**
	 * The sequence sent when resuming the session was invalid. Reconnect and start a new session.
	 */
	INVALID_SEQ = 4007,
	/**
	 * Payloads are being sent too quickly.
	 */
	RATE_LIMITED = 4008,
	/**
	 * Session timed out. Reconnect and start a new one.
	 */
	SESSION_TIMEOUT = 4009,
	/**
	 * Invalid shard sent while identifying.
	 */
	INVALID_SHARD = 4010,
	/**
	 * The session opened would have handled too many guilds.
	 */
	SHARDING_REQUIRED = 4011,
}
