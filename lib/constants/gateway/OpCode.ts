/**
 * Discord Gateway OpCodes
 *
 * @see https://discordapp.com/developers/docs/topics/opcodes-and-status-codes#gateway-gateway-opcodes
 */
export enum OpCode {
	/**
	 * Dispatches an event.
	 *
	 * @direction Receive
	 */
	DISPATCH = 0,
	/**
	 * Used for ping checking.
	 *
	 * @direction Send/Receive
	 */
	HEARTBEAT = 1,
	/**
	 * Used for client handshake.
	 *
	 * @direction Send
	 */
	IDENTIFY = 2,
	/**
	 * Used to update the client status.
	 *
	 * @direction Send
	 */
	STATUS_UPDATE = 3,
	/**
	 * Used to join/move/leave voice channels.
	 *
	 * @direciton Send
	 */
	VOICE_STATE_UPDATE = 4,
	/**
	 * Used to resume a closed connection.
	 *
	 * @direction Send
	 */
	RESUME = 6,
	/**
	 * Used to tell clients to reconnect to the gateway.
	 *
	 * @direciton Receive
	 */
	RECONNECT = 7,
	/**
	 * Used to request guild members.
	 *
	 * @direction Send
	 */
	REQUEST_GUILD_MEMBERS = 8,
	/**
	 * Used to notify client they have an invalid session id.
	 *
	 * @direction Receive
	 */
	INVALID_SESSION = 9,
	/**
	 * Sent immediately after connecting, contains heartbeat and server debug information.
	 *
	 * @direction Receive
	 */
	HELLO = 10,
	/**
	 * Sent immediately following a client heartbeat that was received.
	 *
	 * @direction Receive
	 */
	HEARTBEAT_ACK = 11,
}
