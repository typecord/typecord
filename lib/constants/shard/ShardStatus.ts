/**
 * Shard status enum.
 */
export enum ShardStatus {
	/**
	 * Disconnected from the Discord gateway WebSocket.
	 */
	DISCONNECTED = 0,
	/**
	 * Attempting to connect to the Discord gateway WebSocket.
	 */
	CONNECTING = 1,
	/**
	 * Attempting to handshake with the Discord gateway.
	 */
	HANDSHAKING = 2,
	/**
	 * Hello packet has been received, continuing to identify with the Discord gateway.
	 */
	IDENTIFYING = 3,
	/**
	 * Connection is established and ready.
	 */
	READY = 4,
	/**
	 * Attempting to resume the Discord gateway connection.
	 */
	RESUMING = 5,
}

// tslint:disable-next-line: variable-name
export const ShardStatuses = Object.entries(ShardStatus);
