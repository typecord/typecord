import { GenericError } from '@ayana/errors';

import { CloseCode } from '../constants/gateway';

export class GatewayCloseError extends GenericError {
	public readonly closeCode: CloseCode;
	public readonly reason: string;
	public readonly cleanClose: boolean;

	public constructor(closeCode: CloseCode, reason: string, cleanClose: boolean) {
		super(`Gateway closed: ${reason} (Code: ${closeCode}, Clean: ${cleanClose})`);

		this.__define('closeCode', closeCode);
		this.__define('reason', reason);
		this.__define('cleanClose', cleanClose);
	}
}
