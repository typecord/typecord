import * as WebSocket from 'ws';

import { IllegalStateError } from '@ayana/errors';
import Logger from '@ayana/logger-api';

import { CloseCode, OpCode } from '../constants/gateway';
import { ShardStatus, ShardStatuses } from '../constants/shard';
import { GatewayCloseError } from '../error';
import { Shard, ShardManager } from '../shard';
import { CommandPayload, DispatchPayload, EventPayload, StatusUpdatePayload } from '../types/payloads';
import { SimpleLimiter } from '../util';

import { Compression } from './compression';
import { Heartbeat } from './Heartbeat';
import { Serialization } from './serialization';
import { Session } from './Session';

const log = Logger.get('Gateway');

export class Gateway {
	public readonly shard: Shard;
	public readonly manager: ShardManager;

	private readonly onDispatch: (payload: DispatchPayload) => void;

	public readonly heartbeat: Heartbeat = new Heartbeat(this);
	public readonly session: Session = new Session(this);

	public status: ShardStatus;

	private ws: WebSocket;
	private compression: Compression;
	private serialization: Serialization;

	private connectTimeout: NodeJS.Timer = null;
	private connectAttempts: number = 0;
	private reconnectInterval: number = 1000;

	private readonly sendLimiter: SimpleLimiter<CommandPayload> = new SimpleLimiter<CommandPayload>(120, 60000, payload => {
		if (this.ws && this.ws.readyState === WebSocket.OPEN) {
			this.ws.send(this.serialization.encode(payload));
		}
	});

	private readonly presenceUpdateLimiter: SimpleLimiter<StatusUpdatePayload> = new SimpleLimiter<StatusUpdatePayload>(5, 60000, payload => {
		this.sendLimiter.enqueue(payload);
	});

	public constructor(shard: Shard, manager: ShardManager, onDispatch: (payload: DispatchPayload) => void) {
		this.shard = shard;
		this.manager = manager;

		this.onDispatch = onDispatch;

		this.onWebSocketOpen = this.onWebSocketOpen.bind(this);
		this.onWebSocketMessage = this.onWebSocketMessage.bind(this);
		this.onWebSocketError = this.onWebSocketError.bind(this);
		this.onWebSocketClose = this.onWebSocketClose.bind(this);
		this.onRawPayload = this.onRawPayload.bind(this);
	}

	public connect() {
		if (this.ws != null && this.ws.readyState !== WebSocket.CLOSED) {
			this.manager.emit('error', new IllegalStateError('Cannot connect while a WebSocket still exists'), this.shard.id);

			return;
		}

		this.reset();
		this.updateStatus(ShardStatus.CONNECTING);

		log.trace(`Connecting to Discord gateway with url: ${this.manager.gatewayUrl}`, `${this.shard.id}`);

		this.connectAttempts++;

		this.serialization = Serialization.create(this.manager.options.erlpack ? 'erlpack' : 'json');

		if (typeof this.manager.options.compression === 'string') {
			this.compression = Compression.create(this.manager.options.compression, this.onRawPayload, console.log);
		}

		this.ws = new WebSocket(this.manager.gatewayUrl, null); // TODO: Allow for options

		this.connectTimeout = setTimeout(() => {
			this.disconnect('auto', new IllegalStateError(`Connection timeout: Received no HELLO from Discord after ${this.manager.options.connectTimeout}ms`));
		}, this.manager.options.connectTimeout);

		this.ws.onopen = this.onWebSocketOpen;
		this.ws.onmessage = this.onWebSocketMessage;
		this.ws.onerror = this.onWebSocketError;
		this.ws.onclose = this.onWebSocketClose;
	}

	public disconnect(reconnect: boolean | 'auto', error?: Error) {
		if (this.ws == null) return;

		this.heartbeat.stop();

		// Prevent the onclose handler from being called
		this.ws.onclose = undefined;

		try {
			if (reconnect && this.session.hasSession()) {
				this.ws.terminate();
			} else {
				this.ws.close(CloseCode.NORMAL);
			}
		} catch (e) {
			// TODO: Wrap
			this.manager.emit('error', e, this.shard.id);
		}

		this.manager.emit('disconnect', error || null, this.shard.id);

		this.ws.removeAllListeners();
		this.ws = null;

		if (reconnect === 'auto' && this.manager.options.autoReconnect) {
			this.reset();

			log.debug(`Attempting reconnect after ${this.reconnectInterval}ms (Attempt ${this.connectAttempts + 1})`, `${this.shard.id}`);

			setTimeout(() => {
				// TODO: Reconnect over shard manager so we handle the identify ratelimit
				this.connect();
			}, this.reconnectInterval);

			this.reconnectInterval = Math.min(Math.round(this.reconnectInterval * (Math.random() * 2 + 1)), 30000);
		} else if (!reconnect) {
			this.destroy();
		}
	}

	public send(payload: CommandPayload) {
		if (this.ws && this.ws.readyState === WebSocket.OPEN) {
			if (payload.op === OpCode.STATUS_UPDATE) {
				// This limiter enqueues the payload to the send limiter afterwards
				this.presenceUpdateLimiter.enqueue(payload);
			} else {
				this.sendLimiter.enqueue(payload);
			}
		}
	}

	public destroy() {
		if (this.ws != null) {
			// The call to disconnect will call this function again after the socket has been closed
			this.disconnect(false);

			return;
		}

		this.updateStatus(ShardStatus.DISCONNECTED);
		this.resetConnectTimeout(true);

		this.compression = null; // TODO: Close zlib?
		this.serialization = null;

		this.heartbeat.stop();
		this.session.destroy();
		this.sendLimiter.clear();
		this.presenceUpdateLimiter.clear();

		// TODO: Copy presence?
	}

	private reset() {
		this.updateStatus(ShardStatus.DISCONNECTED);
		this.resetConnectTimeout(false);
		this.heartbeat.stop();
	}

	private onWebSocketOpen() {
		this.updateStatus(ShardStatus.HANDSHAKING);

		this.manager.emit('connect', this.shard.id);
	}

	private onWebSocketError(event: { error: any, message: string, type: string }) {
		// TODO: Nicer error & Wrap
		this.manager.emit('error', event, this.shard.id);
	}

	private onWebSocketClose(event: { wasClean: boolean, code: number, reason: string }) {
		if (event.code == null) event.code = CloseCode.NO_CODE;

		let err = null;
		let reconnect: boolean | 'auto' = 'auto';

		log.debug(`${event.wasClean ? 'Clean' : 'Unclean'} WebSocket close: ${event.code}: ${event.reason}`, `${this.shard.id}`);

		if (event.code !== CloseCode.NO_CODE && event.code !== CloseCode.NORMAL) {
			err = new GatewayCloseError(event.code, event.reason, event.wasClean);
		} else if (!event.wasClean) {
			err = new GatewayCloseError(event.code, event.reason == null ? 'Unknown' : event.reason, false);
		}

		if (
			event.code === CloseCode.AUTHENTICATION_FAILED ||
			event.code === CloseCode.INVALID_SHARD ||
			event.code === CloseCode.SHARDING_REQUIRED ||
			event.code === CloseCode.INVALID_SEQ ||
			event.code === CloseCode.SESSION_TIMEOUT ||
			event.code === CloseCode.RESUME_INVALID_SESSION
		) {
			reconnect = false;
		}

		this.disconnect(reconnect, err);
	}

	/**
	 * Executed when a WebSocket message is received.
	 *
	 * @param event The event containing the received data
	 */
	private onWebSocketMessage(event: { data: WebSocket.Data }): void {
		try {
			if (this.compression != null) {
				// ZLib will callback to onRawPayload when it flushes
				this.compression.add(event.data);
			} else {
				this.onRawPayload(event.data);
			}
		} catch (e) {
			// TODO: Wrap
			this.manager.emit('error', e, this.shard.id);
		}
	}

	/**
	 * Executed when a raw payload is received.
	 * A raw payload is the decompressed buffer or string that contains exactly one payload.
	 *
	 * @param rawPayload The raw payload that should be processed
	 */
	private onRawPayload(rawPayload: string | Buffer | Array<Buffer> | ArrayBuffer) {
		try {
			this.onPayload(this.serialization.decode(rawPayload));
		} catch (e) {
			// TODO: Wrap
			this.manager.emit('error', e, this.shard.id);
		}
	}

	private onPayload(payload: EventPayload) {
		if (this.manager.listenerCount('rawPayload') > 0) {
			this.manager.emit('rawPayload', payload, this.shard.id);
		}

		if (payload.op === OpCode.DISPATCH && payload.s != null) {
			this.session.updateSeq(payload.s, this.status === ShardStatus.RESUMING);
		}

		switch (payload.op) {
			case OpCode.DISPATCH: {
				this.onDispatch(payload);

				if (payload.t !== 'READY' && payload.t !== 'RESUMED') break;

				this.resetConnectTimeout(true);

				if (payload.t === 'RESUMED') {
					log.trace(`Resume from Discord`);

					this.updateStatus(ShardStatus.READY);

					this.manager.emit('shardResume', this.shard.id);
					break;
				}

				log.trace(`Ready from Discord => Trace: ${payload.d._trace}`, `${this.shard.id}`);

				// TODO: Save trace

				this.session.setID(payload.d.session_id);

				this.updateStatus(ShardStatus.READY);

				this.manager.emit('shardReady', this.shard.id);

				break;
			}
			case OpCode.HEARTBEAT: {
				this.heartbeat.send();
				break;
			}
			case OpCode.INVALID_SESSION: {
				log.debug(`Invalid session while resuming. Reidentifying...`);

				this.session.destroy();

				this.updateStatus(ShardStatus.IDENTIFYING);
				this.session.identify();
				break;
			}
			case OpCode.RECONNECT: {
				log.trace(`Reconnect from Discord`, `${this.shard.id}`);
				this.disconnect('auto');
				break;
			}
			case OpCode.HELLO: {
				log.trace(`Hello from Discord => Trace: ${payload.d._trace}, Interval: ${payload.d.heartbeat_interval}`, `${this.shard.id}`);

				// TODO: Save trace

				this.heartbeat.start(payload.d.heartbeat_interval);

				this.resetConnectTimeout(false);

				if (this.session.hasSession()) {
					this.updateStatus(ShardStatus.RESUMING);
					this.session.resume();
				} else {
					this.updateStatus(ShardStatus.IDENTIFYING);
					this.session.identify();
				}

				this.heartbeat.send();

				break;
			}
			case OpCode.HEARTBEAT_ACK: {
				this.heartbeat.ack();
				break;
			}
			default: {
				// TODO: Emit ig?
				log.warn(`Received unknown OpCode ${(payload as any).op}.`);
				break;
			}
		}
	}

	private updateStatus(status: ShardStatus) {
		log.trace(() => `Status update: ${ShardStatuses.find(v => v[1] === status).join(' (')})`, `${this.shard.id}`);

		this.status = status;
	}

	private resetConnectTimeout(resetAttempts: boolean) {
		if (resetAttempts) {
			this.connectAttempts = 0;
			this.reconnectInterval = 1000;
		}

		if (this.connectTimeout != null) {
			clearTimeout(this.connectTimeout);
			this.connectTimeout = null;
		}
	}
}
