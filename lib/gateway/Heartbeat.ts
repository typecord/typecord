import Logger from '@ayana/logger-api';

import { OpCode } from '../constants/gateway';

import { Gateway } from './Gateway';

const log = Logger.get('Heartbeat');

export class Heartbeat {
	private readonly gateway: Gateway;

	private interval: NodeJS.Timer;
	private lastAck: boolean;
	private lastSent: number;
	private lastReceived: number;

	public get latency() {
		return this.lastSent && this.lastReceived ?
			this.lastReceived - this.lastSent : Infinity;
	}

	public constructor(gateway: Gateway) {
		this.gateway = gateway;

		this.stop();
	}

	public stop() {
		if (this.interval != null) {
			clearTimeout(this.interval);
			this.interval = null;
		}

		this.lastAck = true;
		this.lastSent = 0;
		this.lastReceived = 0;
	}

	public start(interval: number) {
		if (interval > 0) {
			if (this.interval != null) clearInterval(this.interval);

			this.interval = setInterval(() => this.send(true), interval);
		} else {
			log.warn(`Looks like Discord does not want us to send heartbeats`, `${this.gateway.shard.id}`);
		}
	}

	public ack() {
		this.lastAck = true;
		this.lastReceived = Date.now();

		log.trace(() => `Received heartbeat ack => Latency: ${this.latency}ms`, `${this.gateway.shard.id}`);
	}

	public send(checkAck: boolean = false) {
		log.trace(() => `Sending heartbeat => Check-Ack: ${checkAck}, Seq: ${this.gateway.session.getSeq()}`, `${this.gateway.shard.id}`);

		if (checkAck && !this.lastAck) {
			// TODO Disconnect (& reconnect ig) because Discord didn't ack our heartbeat
		}

		this.lastAck = false;
		this.lastSent = Date.now();

		this.gateway.send({
			op: OpCode.HEARTBEAT,
			d: this.gateway.session.getSeq(),
		});
	}
}
