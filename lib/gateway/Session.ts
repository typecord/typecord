import Logger from '@ayana/logger-api';

import { OpCode } from '../constants/gateway';

import { Gateway } from './Gateway';

const log = Logger.get('Session');

export class Session {
	private readonly gateway: Gateway;

	private sessionID: string = null;
	private seq: number = 0;

	public constructor(gateway: Gateway) {
		this.gateway = gateway;
	}

	public destroy() {
		this.sessionID = null;
		this.seq = 0;

		log.trace('Destroyed session', `${this.gateway.shard.id}`);
	}

	public updateSeq(seq: number, skipCheck: boolean = false) {
		if (seq > this.seq + 1 && !skipCheck) {
			log.warn(`Non-consecutive sequence (${this.seq} => ${seq})`, `${this.gateway.shard.id}`);
		}
		this.seq = seq;
	}

	public setID(sessionID: string) {
		this.sessionID = sessionID;
	}

	public getSeq() {
		return this.seq;
	}

	public hasSession() {
		return this.sessionID != null;
	}

	public identify() {
		const identify: any = {
			token: this.gateway.manager.token,
			compress: Boolean(this.gateway.manager.options.compression),
			large_threshold: this.gateway.manager.options.largeThreshold,
			properties: {
				$os: process.platform,
				$browser: 'Typecord',
				$device: 'Typecord',
			},
		};

		if (this.gateway.manager.options.shardCount > 1) {
			identify.shard = [this.gateway.shard.id, this.gateway.manager.options.shardCount];
		}
		// if (this.presence.status) {
		// 	identify.presence = this.presence;
		// }

		log.trace(`Identifying => Shard: ${identify.shard ? `${identify.shard[0]}/${identify.shard[1]}` : '0/1'}, Compress: ${identify.compress}, Large Threshold: ${identify.large_threshold}`, `${this.gateway.shard.id}`);

		this.gateway.send({
			op: OpCode.IDENTIFY,
			d: identify,
		});
	}

	public resume() {
		// TODO
	}
}
