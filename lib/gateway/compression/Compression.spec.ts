import '@ayana/test';

import { Compression } from './Compression';
import { ZLibBuiltIn } from './ZLibBuiltIn';
import { ZLibSync } from './ZLibSync';

// tslint:disable: no-empty

autoDescribe(function() {
	describe('#create', function() {
		it('should return an instance of ZLibBuiltIn when zlib is passed as provider', function() {
			const onData = () => {};
			const onError = () => {};

			const instance = Compression.create('zlib', onData, onError);

			expect(instance instanceof ZLibBuiltIn, 'to be true');
		});

		it('should return an instance of ZLibSync if zlib-sync is passed as provider', function() {
			const onData = () => {};
			const onError = () => {};

			const instance = Compression.create('zlib-sync', onData, onError);

			expect(instance instanceof ZLibSync, 'to be true');
		});

		it('should throw an error if no valid zlib provider was passed', function() {
			expect(() => Compression.create('invalid-zlib', null, null), 'to throw', 'Invalid compression provider: invalid-zlib');
		});
	});
});
