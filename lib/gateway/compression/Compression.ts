import { IllegalArgumentError } from '@ayana/errors';

/**
 * Abstract class for all compression handlers.
 */
export abstract class Compression {
	/**
	 * Callback called whenever uncompressed data is available form the handler.
	 */
	protected onData: (data: Buffer) => void;
	/**
	 * Callback called whenever an error happens in the handler.
	 * A handler will not throw errors on method calls.
	 */
	protected onError: (error: Error) => void;

	/**
	 * Creates a new Compression instance.
	 *
	 * @param onData The data callback called whenever uncompressed data is available form the handler
	 * @param onError The error callback called whenever an error happens in the handler
	 */
	protected constructor(onData: (data: Buffer) => void, onError: (error: Error) => void) {
		this.onData = onData;
		this.onError = onError;

		this.init();
	}

	/**
	 * Initializes the handler. This is called in the constructor right after the callbacks are set.
	 */
	protected abstract init(): void;

	/**
	 * Adds compressed data to this compression handler.
	 *
	 * @param data The data to be added
	 */
	public abstract add(data: string | Buffer | Array<Buffer> | ArrayBuffer): void;

	/**
	 * Returns a new instance of a compression handler with the given provider.
	 *
	 * @param provider The compression provider. Currently 'zlib' and 'zlib-sync' are supported
	 * @param onData The data callback called whenever uncompressed data is available form the handler
	 * @param onError The error callback called whenever an error happens in the handler
	 *
	 * @throws IllegalArgumentError => When the compression provider is invalid
	 *
	 * @returns A new instance of a compression handler with the given provider
	 */
	public static create(provider: string, onData: (data: Buffer) => void, onError: (error: Error) => void): Compression {
		if (provider === 'zlib') {
			return new (require('./ZLibBuiltIn')).ZLibBuiltIn(onData, onError);
		} else if (provider === 'zlib-sync') {
			// TODO: Make a check so the user gets a more friendly error message if the package does not exist
			return new (require('./ZLibSync')).ZLibSync(onData, onError);
		}

		throw new IllegalArgumentError(`Invalid compression provider: ${provider}`);
	}
}
