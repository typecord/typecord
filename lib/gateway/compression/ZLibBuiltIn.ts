import { constants, createUnzip, Unzip } from 'zlib';

import { ProcessingError } from '@ayana/errors';

import { Compression } from './Compression';

/**
 * ZLib compression handler using the built-in node zlib module.
 */
export class ZLibBuiltIn extends Compression {
	/**
	 * The unzip instance.
	 *
	 * @see https://nodejs.org/api/zlib.html#zlib_class_zlib_unzip
	 */
	private zlib: Unzip;

	/**
	 * Decompressed data chunks returned from the unzip instance.
	 */
	private readonly zlibChunks: Array<Buffer> = [];
	/**
	 * Temporary storage of compressed data chunks while zlib is flushing.
	 */
	private readonly zlibIncomingChunks: Array<Buffer> = [];
	/**
	 * Indicator whether zlib is currently flushing or not.
	 */
	private zlibFlushing: boolean = false;

	protected init() {
		this.flush = this.flush.bind(this);

		this.zlib = createUnzip({
			flush: constants.Z_SYNC_FLUSH,
			chunkSize: 128 * 1024,
		}).on('data', data => {
			this.zlibChunks.push(data);
		}).on('error', e => {
			this.onError(e);
		});
	}

	public add(data: Buffer | Array<Buffer> | ArrayBuffer) {
		if (data instanceof Buffer) {
			this.addBuffer(data);
		} else if (Array.isArray(data)) {
			console.log('ZLibBuiltIn: RECEIVED Fragmented Buffer message');
			data.forEach(d => this.addBuffer(d));
		} else if (data instanceof ArrayBuffer) {
			console.log('ZLibBuiltIn: RECEIVED ArrayBuffer message');
			this.addBuffer(Buffer.from(data));
		} else {
			this.onError(new ProcessingError(`ZLibBuiltIn: Received invalid data`));
		}
	}

	private addBuffer(data: Buffer) {
		if (this.zlibFlushing) {
			this.zlibIncomingChunks.push(data);
		} else {
			this.writeData(data);
		}
	}

	/**
	 * Called whenever zlib finishes flushing.
	 */
	private flush() {
		this.zlibFlushing = false;

		if (this.zlibChunks == null || this.zlibChunks.length === 0) return;

		// Concat all chunks to one buffer
		let buffer = this.zlibChunks[0];
		if (this.zlibChunks.length > 1) {
			buffer = Buffer.concat(this.zlibChunks);
		}

		// Clear array
		this.zlibChunks.length = 0;

		// Process incoming chunks until zlib starts flushing again
		while (this.zlibIncomingChunks.length > 0) {
			if (this.writeData(this.zlibIncomingChunks.shift())) break;
		}

		this.onData(buffer);
	}

	/**
	 * Writes data to the zlib unzip and initiates flushing if all data has been received.
	 *
	 * @param data The data to write as a Buffer
	 *
	 * @returns *true* when flushing was initiated and *false* if otherwise
	 */
	private writeData(data: Buffer): boolean {
		this.zlib.write(data);

		if (data.length >= 4 && data.readUInt32BE(data.length - 4) === 0xFFFF) {
			this.zlibFlushing = true;
			this.zlib.flush(constants.Z_SYNC_FLUSH, this.flush);

			return true;
		}

		return false;
	}
}
