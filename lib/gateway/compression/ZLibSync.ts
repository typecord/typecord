/// <reference types="../../../typings/zlib-sync" />

// tslint:disable-next-line: no-implicit-dependencies
import { Inflate, Z_SYNC_FLUSH } from 'zlib-sync';

import { ProcessingError } from '@ayana/errors';

import { Compression } from './Compression';

/**
 * ZLib compression handler using the external zlib-sync module.
 */
export class ZLibSync extends Compression {
	/**
	 * The inflate instance.
	 */
	private zlib: Inflate;

	protected init() {
		this.zlib = new Inflate({
			chunkSize: 128 * 1024,
		});
	}

	public add(data: Buffer | Array<Buffer> | ArrayBuffer) {
		if (data instanceof Buffer) {
			this.addBuffer(data);
		} else if (Array.isArray(data)) {
			console.log('ZLibSync: RECEIVED Fragmented Buffer message');
			data.forEach(d => this.addBuffer(d));
		} else if (data instanceof ArrayBuffer) {
			console.log('ZLibSync: RECEIVED ArrayBuffer message');
			this.addBuffer(Buffer.from(data));
		} else {
			this.onError(new ProcessingError(`ZLibSync: Received invalid data`));
		}
	}

	private addBuffer(data: Buffer) {
		if (data.length >= 4 && data.readUInt32BE(data.length - 4) === 0xFFFF) {
			this.zlib.push(data, Z_SYNC_FLUSH);

			if (this.zlib.err) {
				this.onError(new ProcessingError(`ZLibSync: Error ${this.zlib.err}: ${this.zlib.msg}`));

				return;
			}

			this.onData(Buffer.from(this.zlib.result));
		} else {
			this.zlib.push(data);
		}
	}
}
