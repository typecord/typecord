/// <reference types="../../../typings/erlpack" />

// tslint:disable-next-line: no-implicit-dependencies
import { pack, unpack } from 'erlpack';

import { ProcessingError } from '@ayana/errors';

import { CommandPayload, EventPayload } from '../../types/payloads';

import { Serialization } from './Serialization';

/**
 * Serialization handler for the ETF format using the erlpack library.
 */
export class Erlpack extends Serialization {
	public encode(payload: CommandPayload): Buffer {
		return pack(payload);
	}

	public decode(rawPayload: string | Buffer | Array<Buffer> | ArrayBuffer): EventPayload {
		if (rawPayload instanceof Buffer) {
			return unpack(rawPayload);
		} else if (Array.isArray(rawPayload)) {
			console.log('ErlpackFormatter: Received fragmented Buffer message');

			return unpack(Buffer.concat(rawPayload));
		} else if (rawPayload instanceof ArrayBuffer) {
			console.log('ErlpackFormatter: Received ArrayBuffer message');

			return unpack(Buffer.from(rawPayload));
		}

		throw new ProcessingError(`Erlpack: Received invalid data`);
	}
}
