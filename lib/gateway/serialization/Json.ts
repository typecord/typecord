import { ProcessingError } from '@ayana/errors';

import { CommandPayload, EventPayload } from '../../types/payloads';

import { Serialization } from './Serialization';

/**
 * Serialization handler for the JSON format using the built-in JSON.stringify and JSON.parse functions.
 */
export class Json extends Serialization {
	public encode(payload: CommandPayload): string {
		return JSON.stringify(payload);
	}

	public decode(rawPayload: string | Buffer | Array<Buffer> | ArrayBuffer): EventPayload {
		if (
			typeof rawPayload === 'string' ||
			rawPayload instanceof Buffer ||
			rawPayload instanceof ArrayBuffer
		) {
			return JSON.parse(rawPayload.toString());
		}

		if (Array.isArray(rawPayload)) {
			console.log('Json: RECEIVED fragmented Buffer message');
			rawPayload = Buffer.concat(rawPayload);
		}

		throw new ProcessingError(`Json: Received invalid data`);
	}
}
