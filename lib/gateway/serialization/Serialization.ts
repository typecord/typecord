import { IllegalArgumentError } from '@ayana/errors';

import { CommandPayload, EventPayload } from '../../types/payloads';

/**
 * Abstract class for all serialization handlers.
 */
export abstract class Serialization {
	/**
	 * Serializes a payload to a Buffer or string that can be sent over a WebSocket connection.
	 *
	 * @param payload The gateway payload that should be encoded
	 *
	 * @returns A Buffer or string containing the encoded data
	 */
	public abstract encode(payload: CommandPayload): Buffer | string;

	/**
	 * Deserializes a WebSocket data packet to a payload.
	 *
	 * @param rawPayload The received and decompressed WebSocket data packet
	 *
	 * @returns A gateway payload containing the deserialized data
	 */
	public abstract decode(rawPayload: string | Buffer | Array<Buffer> | ArrayBuffer): EventPayload;

	/**
	 * Returns a new instance of a serialization handler with the given provider.
	 *
	 * @param provider The compression provider. Currently 'json' and 'erlpack' are supported
	 *
	 * @throws IllegalArgumentError => When the compression provider is invalid
	 *
	 * @returns A new instance of a serialization handler with the given provider
	 */
	public static create(provider: string): Serialization {
		if (provider === 'json') {
			return new (require('./Json')).Json();
		} else if (provider === 'erlpack') {
			// TODO: Make a check so the user gets a more friendly error message if the package does not exist
			return new (require('./Erlpack')).Erlpack();
		}

		throw new IllegalArgumentError(`Invalid serialization provider: ${provider}`);
	}
}
