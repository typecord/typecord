import Logger from '@ayana/logger-api';

import { Gateway } from '../gateway';
import { DispatchPayload } from '../types/payloads';

import { ShardManager } from './ShardManager';

const log = Logger.get('Shard');

export class Shard {
	public readonly id: number;
	private readonly manager: ShardManager;

	private readonly gateway: Gateway;

	private readonly dispatchFn: (payload: DispatchPayload) => void;

	public constructor(id: number, manager: ShardManager, onDispatch: (payload: DispatchPayload) => void) {
		this.id = id;
		this.manager = manager;

		this.dispatchFn = onDispatch;

		this.gateway = new Gateway(this, manager, this.onDispatch.bind(this));
	}

	public getStatus() {
		return this.gateway.status;
	}

	public connect() {
		if (this.manager.token == null) {
			// TODO nicer error, maybe emit?
			throw new Error('Token not specified');
		}

		this.gateway.connect();
	}

	private onDispatch(payload: DispatchPayload) {
		this.dispatchFn(payload);
	}
}
