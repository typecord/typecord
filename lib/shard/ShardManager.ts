import { EventEmitter } from 'events';

import Logger from '@ayana/logger-api';

import { GATEWAY_VERSION } from '../constants/gateway';
import { ShardStatus } from '../constants/shard';

import { Shard } from './Shard';

export interface ShardManagerOptions {
	compression?: boolean | 'zlib' | 'zlib-sync';
	erlpack?: boolean;
	connectTimeout?: number;
	largeThreshold?: number;
	shardCount?: number;
	autoReconnect?: boolean;
}

const log = Logger.get('ShardManager');

export class ShardManager extends EventEmitter {
	public readonly options: ShardManagerOptions;
	public readonly token: string;

	private readonly shards: Map<number, Shard> = new Map();

	private _gatewayUrl: string;

	public get gatewayUrl() {
		return this._gatewayUrl;
	}

	public constructor(token: string, options?: ShardManagerOptions) {
		super();

		this.options = {
			compression: false,
			erlpack: false,
			connectTimeout: 30000,
			largeThreshold: 250,
			shardCount: 1,
			autoReconnect: true,
		};

		this.token = token;

		Object.assign(this.options, options);

		if (this.options.compression === true) this.options.compression = 'zlib';
	}

	public async connect() {
		// TODO Get this from REST
		const baseUrl = `https://gateway.discord.gg/`;

		this._gatewayUrl = `${baseUrl}?v=${GATEWAY_VERSION}`;

		if (this.options.compression) {
			this._gatewayUrl += `&compress=zlib-stream`;
			log.debug(`zLib compression is enabled (Library: ${this.options.compression})`);
		} else {
			log.debug(`zLib compression is disabled`);
		}

		if (this.options.erlpack) {
			this._gatewayUrl += `&encoding=etf`;
			log.debug(`Using Erlpack for message decoding`);
		} else {
			this._gatewayUrl += `&encoding=json`;
			log.debug(`Using JSON for message decoding`);
		}
	}

	public spawn(id: number) {
		if (!this.shards.has(id)) {
			// TODO: this.shards.set(id, new Shard(id, this));
		}

		if (this.shards.get(id).getStatus() === ShardStatus.DISCONNECTED) {
			// TODO: Connect
		}
	}
}
