/**
 * Discord Identify Event.
 * Used to trigger the initial handshake with the gateway.
 *
 * @see https://discordapp.com/developers/docs/topics/gateway#identify
 */
export interface IdentifyEvent {
	/**
	 * Discord authentication token.
	 */
	token: string;
	/**
	 * Connection properties.
	 */
	properties?: {
		/**
		 * The current operating system.
		 */
		$os: string;
		/**
		 * The librarys name.
		 */
		$browser: string;
		/**
		 * The librarys name.
		 */
		$device: string;
	};
	/**
	 * Whether this connection supports compression of packets.
	 */
	compress?: boolean;
}
