export interface ResumeEvent {
	token: string;
	session_id: string;
	seq: number;
}
