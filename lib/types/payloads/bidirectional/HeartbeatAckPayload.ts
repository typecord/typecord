import { OpCode } from '../../../constants/gateway';

/**
 * Bi-Directional payload to tell the other side that the heartbeat got acknowledged.
 *
 * @direction Send/Receive
 */
export interface HeartbeatAckPayload {
	/**
	 * The OpCode.
	 */
	op: OpCode.HEARTBEAT_ACK;
}
