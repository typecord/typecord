import { OpCode } from '../../../constants/gateway';

/**
 * Bi-Directional payload to tell the other side that it should send a heartbeat ack.
 *
 * @direction Send/Receive
 */
export interface HeartbeatPayload {
	/**
	 * The OpCode.
	 */
	op: OpCode.HEARTBEAT;
	/**
	 * The current sequence number.
	 */
	d: number;
}
