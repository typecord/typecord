import { HeartbeatAckPayload } from './HeartbeatAckPayload';
import { HeartbeatPayload } from './HeartbeatPayload';

export {
	HeartbeatAckPayload,
	HeartbeatPayload,
};

/**
 * Discord Gateway payloads that can be sent/received in both directions.
 */
export type BidirectionalPayload = HeartbeatAckPayload | HeartbeatPayload;
