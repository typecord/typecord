import { OpCode } from '../../../constants/gateway';
import { IdentifyEvent } from '../../events';

export interface IdentifyPayload {
	op: OpCode.IDENTIFY;
	d: IdentifyEvent;
}
