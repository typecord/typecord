import { OpCode } from '../../../constants/gateway';

export interface RequestGuildMembersPayload {
	op: OpCode.REQUEST_GUILD_MEMBERS;
}
