import { OpCode } from '../../../constants/gateway';
import { ResumeEvent } from '../../events';

export interface ResumePayload {
	op: OpCode.RESUME;
	d: ResumeEvent;
}
