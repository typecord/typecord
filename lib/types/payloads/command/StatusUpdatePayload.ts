import { OpCode } from '../../../constants/gateway';

export interface StatusUpdatePayload {
	op: OpCode.STATUS_UPDATE;
}
