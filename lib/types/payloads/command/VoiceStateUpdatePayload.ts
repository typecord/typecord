import { OpCode } from '../../../constants/gateway';

export interface VoiceStateUpdatePayload {
	op: OpCode.VOICE_STATE_UPDATE;
}
