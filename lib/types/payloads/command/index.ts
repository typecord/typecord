import { BidirectionalPayload } from '../bidirectional';

import { IdentifyPayload } from './IdentifyPayload';
import { RequestGuildMembersPayload } from './RequestGuildMembersPayload';
import { ResumePayload } from './ResumePayload';
import { StatusUpdatePayload } from './StatusUpdatePayload';
import { VoiceStateUpdatePayload } from './VoiceStateUpdatePayload';

export {
	IdentifyPayload,
	RequestGuildMembersPayload,
	ResumePayload,
	StatusUpdatePayload,
	VoiceStateUpdatePayload,
};

/**
 * Discord Gateway payloads that can only be sent (commands).
 */
export type CommandPayload = BidirectionalPayload | IdentifyPayload | RequestGuildMembersPayload | ResumePayload | StatusUpdatePayload | VoiceStateUpdatePayload;
