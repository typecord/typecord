import { OpCode } from '../../../constants/gateway';

/**
 * Dispatches an event in the client.
 *
 * @direction Receive
 */
export interface DispatchPayload {
	/**
	 * The OpCode.
	 */
	op: OpCode.DISPATCH;
	/**
	 * The event data.
	 */
	d: { [key: string]: any };
	/**
	 * The sequence number, used for resuming sessions and heartbeats.
	 */
	s: number;
	/**
	 * The event name for this payload.
	 */
	t: string;
}
