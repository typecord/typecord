import { OpCode } from '../../../constants/gateway';

export interface HelloPayload {
	op: OpCode.HELLO;
	d: {
		heartbeat_interval: number;
		_trace: Array<string>;
	};
}
