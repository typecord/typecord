import { OpCode } from '../../../constants/gateway';

export interface InvalidSessionPayload {
	op: OpCode.INVALID_SESSION;
	d: boolean;
}
