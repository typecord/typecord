import { OpCode } from '../../../constants/gateway';

/**
 * Not documented anymore.
 */
export interface ReconnectPayload {
	op: OpCode.RECONNECT;
}
