import { BidirectionalPayload } from '../bidirectional';

import { DispatchPayload } from './DispatchPayload';
import { HelloPayload } from './HelloPayload';
import { InvalidSessionPayload } from './InvalidSessionPayload';
import { ReconnectPayload } from './ReconnectPayload';

export {
	DispatchPayload,
	HelloPayload,
	InvalidSessionPayload,
	ReconnectPayload,
};

/**
 * Discord Gateway payloads that can only be received (events).
 */
export type EventPayload = BidirectionalPayload | DispatchPayload | HelloPayload | InvalidSessionPayload | ReconnectPayload;
