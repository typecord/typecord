import { CommandPayload } from './command';
import { EventPayload } from './event';

export * from './bidirectional';
export * from './command';
export * from './event';

/**
 * Discord Gateway payload.
 *
 * @see https://discordapp.com/developers/docs/topics/gateway#payloads
 */
export type Payload = CommandPayload | EventPayload;
