export interface ClientPresence {
	since: number;
	game: any;
	status: string;
}
