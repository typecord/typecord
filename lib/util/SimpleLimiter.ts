/**
 * Allows limiting operations in a certain timespan.
 * Used for limiting sent gateway payloads and presence updates.
 */
export class SimpleLimiter<T> {
	private readonly limit: number;
	private readonly timespan: number;
	private readonly callback: (item: T) => void;

	private readonly queue: Array<T> = [];

	private timeout: NodeJS.Timeout = null;
	private lastReset: number = 0;
	private processed: number = 0;

	/**
	 * Creates a new SimpleLimiter.
	 *
	 * @param limit The amount of operations allowed in one timespan
	 * @param timespan The timespan in milliseconds
	 * @param callback The callback to be called whenever an operation can be executed
	 */
	public constructor(limit: number, timespan: number, callback: (item: T) => void) {
		this.limit = limit;
		this.timespan = timespan;
		this.callback = callback;
	}

	/**
	 * Enqueues an item that gets passed to the callback when an operation is executed.
	 * The queue is processed after the FIFO method.
	 *
	 * @param item The item to be passed
	 */
	public enqueue(item: T) {
		this.queue.push(item);
		this.check();
	}

	/**
	 * Clears all items in this limiter.
	 */
	public clear() {
		if (this.timeout != null) {
			clearTimeout(this.timeout);
		}

		this.queue.length = 0;
	}

	private check() {
		// If we have no items or a timeout is running we should not process items
		if (this.queue.length === 0 || this.timeout != null) return;

		// Check if we can reset the amount of processed items
		if (this.lastReset + this.timespan < Date.now()) {
			this.lastReset = Date.now();
			this.processed = 0;
		}

		// As long as we don't reach the limit we can process items
		while (this.queue.length > 0 && this.processed < this.limit) {
			this.processed++;
			this.callback(this.queue.shift());
		}

		// If we still have items remaining we set a timeout to call this function again later
		if (this.queue.length > 0 && this.timeout == null) {
			this.timeout = setTimeout(() => {
				this.timeout = null;
				this.check();
			}, Math.max(0, this.lastReset + this.timespan - Date.now()));
		}
	}
}
