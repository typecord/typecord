'use strict';

const { Compression } = require('../../../../../build/gateway/compression/Compression');

describe('#constructor', function() {
	it('should set the passed functions as attributes and call the init method', function() {
		class CompressionTest extends Compression {}

		CompressionTest.prototype.init = sinon.fake();

		const tested = new CompressionTest('fn1', 'fn2');

		expect(tested.onData, 'to be', 'fn1');
		expect(tested.onError, 'to be', 'fn2');
		sinon.assert.calledOnce(CompressionTest.prototype.init);
	});
});
