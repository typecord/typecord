'use strict';

const { Compression } = require('../../../../../build/gateway/compression/Compression');
const { ZLibBuiltIn } = require('../../../../../build/gateway/compression/ZLibBuiltIn');
const { ZLibSync } = require('../../../../../build/gateway/compression/ZLibSync');

describe('#create', function() {
	it('should return an instance of ZLibBuiltIn when zlib is passed as provider', function() {
		const instance = Compression.create('zlib', 'somefunc', 'anotherfunc');

		expect(instance instanceof ZLibBuiltIn, 'to be true');
		expect(instance.onData, 'to be', 'somefunc');
		expect(instance.onError, 'to be', 'anotherfunc');
	});

	it('should return an instance of ZLibSync if zlib-sync is passed as provider', function() {
		const instance = Compression.create('zlib-sync', 'somefunc', 'anotherfunc');

		expect(instance instanceof ZLibSync, 'to be true');
		expect(instance.onData, 'to be', 'somefunc');
		expect(instance.onError, 'to be', 'anotherfunc');
	});

	it('should throw an error if no valid zlib provider was passed', function() {
		expect(() => Compression.create('invalid-zlib'), 'to throw', 'Invalid compression provider: invalid-zlib');
	});
});
