'use strict';

const { ZLibBuiltIn } = require('../../../../../build/gateway/compression/ZLibBuiltIn');

describe('#addBuffer', function() {
	const getClean = () => {
		const tested = new ZLibBuiltIn();

		tested.writeData = sinon.fake();

		return tested;
	};

	it('should write the data if zlib is not flushing', function() {
		const tested = getClean();

		// Assert the default value so we fail if that gets changed
		expect(tested.zlibFlushing, 'to be false');

		const buffer = Buffer.from([0, 1, 2, 3]);
		tested.addBuffer(buffer);

		sinon.assert.calledOnce(tested.writeData);
		expect(tested.writeData.args[0][0], 'to be', buffer);
	});

	it('should add the data to a temporary buffer if zlib is flushing', function() {
		const tested = getClean();

		tested.zlibFlushing = true;

		const buffer = Buffer.from([0, 1, 2, 3]);
		tested.addBuffer(buffer);

		sinon.assert.notCalled(tested.writeData);
		expect(tested.zlibIncomingChunks, 'to have length', 1);
		expect(tested.zlibIncomingChunks[0], 'to be', buffer);
	});
});
