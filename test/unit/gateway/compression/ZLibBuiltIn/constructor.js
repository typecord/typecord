'use strict';

const { ZLibBuiltIn } = require('../../../../../build/gateway/compression/ZLibBuiltIn');

describe('#constructor', function() {
	it('should bind the flush function and create a new inflate instance', function() {
		const tested = new ZLibBuiltIn();

		expect(tested.zlib instanceof (require('zlib').Unzip), 'to be true');

		expect(tested.flush, 'not to be', ZLibBuiltIn.prototype.flush);
		expect(typeof tested.flush, 'to be', 'function');
	});
});
