'use strict';

const { ZLibBuiltIn } = require('../../../../../build/gateway/compression/ZLibBuiltIn');

describe('#flush', function() {
	const getClean = () => {
		const tested = new ZLibBuiltIn();

		tested.writeData = sinon.fake();
		tested.onData = sinon.fake();

		return tested;
	};

	it('should set zlib flushing to false', function() {
		const tested = getClean();

		tested.zlibFlushing = true;

		tested.flush();

		expect(tested.zlibFlushing, 'to be false');
	});

	it('should abort when flushing is attempted while no data is there', function() {
		const tested = getClean();

		tested.flush();

		sinon.assert.notCalled(tested.onData);
	});

	it('should pass the buffer if only one buffer is there and clear the chunk array', function() {
		const tested = getClean();

		const buffer = Buffer.from([0, 1, 2, 3]);
		tested.zlibChunks.push(buffer);

		tested.flush();

		sinon.assert.calledOnce(tested.onData);
		expect(tested.onData.args[0][0], 'to be', buffer);
		expect(tested.zlibChunks, 'to have length', 0);
	});

	it('should concat the buffers if multiple buffers exist', function() {
		const tested = getClean();

		const buffer1 = Buffer.from([0, 1, 2, 3]);
		const buffer2 = Buffer.from([3, 2, 1, 0]);
		tested.zlibChunks.push(buffer1, buffer2);

		tested.flush();

		sinon.assert.calledOnce(tested.onData);
		expect(tested.onData.args[0][0], 'not to be', buffer1);
		expect(tested.onData.args[0][0], 'not to be', buffer2);
		expect(tested.onData.args[0][0], 'to have length', 8);
	});

	it('should write incoming chunks as long as there is data and the called funciton returns false', function() {
		const tested = getClean();

		tested.writeData = sinon.fake.returns(false);

		tested.zlibChunks.push('data');
		tested.zlibIncomingChunks.push('a', 'b', 'c');

		tested.flush();

		sinon.assert.calledThrice(tested.writeData);
		expect(tested.writeData.args[0][0], 'to be', 'a');
		expect(tested.writeData.args[1][0], 'to be', 'b');
		expect(tested.writeData.args[2][0], 'to be', 'c');
	});

	it('should abort writing incoming chunks if the called funciton returns true', function() {
		const tested = getClean();

		tested.writeData = sinon.fake.returns(true);

		tested.zlibChunks.push('data');
		tested.zlibIncomingChunks.push('a', 'b', 'c');

		tested.flush();

		sinon.assert.calledOnce(tested.writeData);
		expect(tested.writeData.args[0][0], 'to be', 'a');
	});
});
