'use strict';

const { Z_SYNC_FLUSH } = require('zlib');

const { ZLibBuiltIn } = require('../../../../../build/gateway/compression/ZLibBuiltIn');

describe('#writeData', function() {
	const getClean = () => {
		const tested = new ZLibBuiltIn();

		tested.zlib = {
			write: sinon.fake(),
			flush: sinon.fake(),
		};

		return tested;
	};

	it('should write data and return false if not flushing', function() {
		const tested = getClean();

		const buffer = Buffer.from([0, 1, 2, 3]);

		expect(tested.writeData(buffer), 'to be false');
		sinon.assert.calledOnce(tested.zlib.write);
		expect(tested.zlib.write.args[0][0], 'to be', buffer);
	});

	it('should write data and return true if flushing', function() {
		const tested = getClean();

		const buffer = Buffer.from([0, 0, 255, 255]);

		expect(tested.writeData(buffer), 'to be true');
		expect(tested.zlibFlushing, 'to be true');
		sinon.assert.calledOnce(tested.zlib.flush);
		expect(tested.zlib.flush.args[0][0], 'to be', Z_SYNC_FLUSH);
		expect(tested.zlib.flush.args[0][1], 'to be', tested.flush);
	});
});
