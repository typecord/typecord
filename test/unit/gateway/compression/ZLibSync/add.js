'use strict';

const { ZLibSync } = require('../../../../../build/gateway/compression/ZLibSync');

describe('#add', function() {
	const getClean = () => {
		const tested = new ZLibSync();

		tested.addBuffer = sinon.fake();
		tested.onError = sinon.fake();

		return tested;
	};

	it('should pass the buffer to the addBuffer function', function() {
		const tested = getClean();

		const buffer = Buffer.from([0, 1, 2, 3]);
		tested.add(buffer);

		sinon.assert.calledOnce(tested.addBuffer);
		sinon.assert.notCalled(tested.onError);

		expect(tested.addBuffer.args[0][0], 'to be', buffer);
	});

	it('should pass each buffer to the addBuffer function out of a Buffer array', function() {
		const tested = getClean();

		const buffers = [
			Buffer.from([0, 1, 2, 3]),
			Buffer.from([3, 2, 1, 0]),
		];

		tested.add(buffers);

		sinon.assert.calledTwice(tested.addBuffer);
		sinon.assert.notCalled(tested.onError);

		expect(tested.addBuffer.args[0][0], 'to be', buffers[0]);
		expect(tested.addBuffer.args[1][0], 'to be', buffers[1]);
	});

	it('should convert a given ArrayBuffer to a regular buffer and pass it to the addBuffer function', function() {
		const tested = getClean();

		const buffer = Buffer.from([0, 1, 2, 3]);
		const arrayBuffer = new Uint8Array(buffer).buffer;

		tested.add(arrayBuffer);

		sinon.assert.calledOnce(tested.addBuffer);
		sinon.assert.notCalled(tested.onError);

		expect(buffer.equals(tested.addBuffer.args[0][0]), 'to be true');
	});

	it('should call the error callback when the given value is invalid', function() {
		const tested = getClean();

		tested.add(null);

		sinon.assert.notCalled(tested.addBuffer);
		sinon.assert.calledOnce(tested.onError);

		expect(tested.onError.args[0][0], 'to have message', 'ZLibSync: Received invalid data');
	});
});
