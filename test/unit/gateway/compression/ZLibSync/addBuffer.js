'use strict';

const { ZLibSync } = require('../../../../../build/gateway/compression/ZLibSync');

describe('#addBuffer', function() {
	const getClean = () => {
		const tested = new ZLibSync(sinon.fake(), sinon.fake());

		tested.zlib = {
			push: sinon.fake(),
			err: null,
			msg: null,
			result: 'zlib-result',
		};

		return tested;
	};

	it('should append the buffer', function() {
		const tested = getClean();

		const buffer = Buffer.from([0, 0, 0, 0]);

		tested.addBuffer(buffer);

		sinon.assert.calledOnce(tested.zlib.push);
		expect(tested.zlib.push.args[0][0], 'to be', buffer);
	});

	it('should flush the zlib instance if the last 4 bytes match 0xFFFF and call onData with the result', function() {
		const tested = getClean();

		const buffer = Buffer.from([0, 0, 255, 255]);

		tested.addBuffer(buffer);

		sinon.assert.calledOnce(tested.zlib.push);
		expect(tested.zlib.push.args[0][0], 'to be', buffer);

		sinon.assert.calledOnce(tested.onData);
		expect(tested.onData.args[0][0], 'when decoded as', 'utf-8', 'to be', 'zlib-result');
	});

	it('should error out if flushing the zlib instance fails', function() {
		const tested = getClean();
		tested.zlib.err = 'Some Error';
		tested.zlib.msg = 'Some Message';

		const buffer = Buffer.from([0, 0, 255, 255]);

		tested.add(buffer);

		sinon.assert.calledOnce(tested.zlib.push);
		expect(tested.zlib.push.args[0][0], 'to be', buffer);

		sinon.assert.calledOnce(tested.onError);
		expect(tested.onError.args[0][0], 'to have message', 'ZLibSync: Error Some Error: Some Message');
	});
});
