'use strict';

const { ZLibSync } = require('../../../../../build/gateway/compression/ZLibSync');

describe('#constructor', function() {
	it('should create a new inflate instance', function() {
		const tested = new ZLibSync();

		expect(tested.zlib instanceof (require('zlib-sync').Inflate), 'to be true');
	});
});
