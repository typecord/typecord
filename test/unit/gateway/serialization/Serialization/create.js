'use strict';

const { Serialization } = require('../../../../../build/gateway/serialization/Serialization');
const { Json } = require('../../../../../build/gateway/serialization/Json');
const { Erlpack } = require('../../../../../build/gateway/serialization/Erlpack');

describe('#create', function() {
	it('should return an instance of Json when json is passed as provider', function() {
		const instance = Serialization.create('json');

		expect(instance instanceof Json, 'to be true');
	});

	it('should return an instance of Erlpack if erlpack is passed as provider', function() {
		const instance = Serialization.create('erlpack');

		expect(instance instanceof Erlpack, 'to be true');
	});

	it('should throw an error if no valid zlib provider was passed', function() {
		expect(() => Serialization.create('invalid-serialization'), 'to throw', 'Invalid serialization provider: invalid-serialization');
	});
});
