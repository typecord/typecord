declare module 'erlpack' {
	export function pack(value: any): Buffer;
	export function unpack(data: Buffer): any; 
}
